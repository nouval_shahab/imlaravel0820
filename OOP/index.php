<?php  

require ('frog.php');
require ('ape.php');
require ('animal.php');

$sheep = new Animal("shaun");

#echo $sheep->name; // "shaun"
#echo $sheep->legs; // 2
#echo $sheep->cold_blooded; // false

$sungokong = new Ape("kera sakti");
#$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
#$kodok->jump(); // "hop hop"

$daftar = [
	["Indonesia", "emas"],
	["Indonesia", "perak"],
	['Korea Selatan', "emas"]
];
	
function perolehan_medali($daftar){
	$result = [];
	for ($i = 0; $i < count($daftar); $i+=1){
		$emas = 0;
		$perunggu = 0;
		$perak = 0;
    	foreach ($daftar[$i] as $key=>$item){
    		if ($item == "emas"){
    			$emas += 1;
    		} elseif ($item == "perak"){
    			$perak += 1;
    		} elseif($item == "perunggu") {
    			$perunggu +=1;
    		} else{
    			$negara = $item;
    		}

        }
    	
    	array_push($result, ["negara" => $negara, "emas" => $emas, "perak" => $perak, "perunggu" => $perunggu]);
    }

    $country = [];
	foreach($result as $val) {
		if(array_key_exists($val['negara'], $country)){
			$country[$val['negara']]["emas"] += $val["emas"];
			$country[$val['negara']]["perak"] += $val["perak"];
			$country[$val['negara']]["perunggu"] += $val["perunggu"]; 
		} else{
			$country[$val['negara']]["emas"] = $val["emas"];
			$country[$val['negara']]["perak"] = $val["perak"];
			$country[$val['negara']]["perunggu"] = $val["perunggu"];  
		}
	
	}


    return $country;
}

$result = perolehan_medali($daftar);


print_r($result);
?>