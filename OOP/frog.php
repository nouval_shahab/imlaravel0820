<<?php 

class Frog {
  public $name;
  public $legs = 4;
  public $cold_blooded = "false";

  function __construct($name) {
    $this->name = $name;

  }

  function jump(){
  	echo "Hop Hop!";
  }

}

?>
