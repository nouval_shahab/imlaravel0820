<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
   <?php
        function ubah_huruf($string){
        
            $daftar = "abcdefghijklmnopqrstuvwxyz";
            $new = "";

            for ($i = 0; $i < strlen($string); $i++){
                $pos = strpos($daftar, $string[$i]);
                
                if ($pos == 25){
                    $new .= $daftar[0];
                }
                else{
                    $new .= $daftar[$pos + 1];
                }
            }
            
            return $new;

        }

        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu
        echo ubah_huruf("zuzuzu");
    ?>
</body>
</html>
