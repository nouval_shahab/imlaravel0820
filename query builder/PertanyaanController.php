<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{

    public function create(){
    	return view('pertanyaan.create');
    }

    public funtion store(Request, $Requests){
    	$query = DB::table('pertanyaan')->insert(
			['judul' => $requests["judul"], 
			'isi' => $requests["isi"]
		]
		);

    	return redirect('/pertanyaan')->with('success', 'Berhasil memposting pertanyaan');

    }

    public function index(){
    	$posts = DB::table('pertanyaan')->get();
    	return view('pertanyaan.index', compact($posts));
    }

    public function show($id){
    	$posts = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('pertanyaan.show', compact($posts));
    }

    public function edit($id){
        $posts = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact($posts));
    }

    public function update($id, Request $requests){
    	$query = DB::table('pertanyaan')
    		->where('id', $id)
    		->update(['judul' => $requests["judul"], 
			'isi' => $requests["isi"]
			]);

		return redirect('/pertanyaan')->with('success', 'Berhasil mengedit pertanyaan');
    }

    public function destroy($id){
    	$query = DB::table('pertanyaan')->where('id', $id)->delete();

		return redirect('/pertanyaan')->with('success', 'Berhasil menghapus pertanyaan');

    }
}
